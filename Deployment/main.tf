resource "aws_vpc" "main" {
  // The cird block for vpc
  cidr_block       = var.vpc_cird_base

  // A tenancy option for instances launched into the VPC
  instance_tenancy = var.instance_tenancy

  // A boolean flag to enable/disable DNS support in the VPC. 
  enable_dns_support = var.enable_dns_support

  // A boolean flag to enable/disable DNS hostnames in the VPC.
  enable_dns_hostnames = var.enable_dns_hostnames

  // A boolean flag to enable/disable ClassicLink for the VPC. Only valid in regions and accounts that support EC2 Classic.
  enable_classiclink = var.enable_classiclink

  tags = {
    Name = var.aws_vpc_name
  }
}

resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.main.id
  tags = {
      Name = var.aws_vpc_name
    }
}


resource "aws_subnet" "subnet_public" {
  vpc_id = aws_vpc.main.id
  cidr_block = var.cidr_subnet
  map_public_ip_on_launch = "true"
  availability_zone = var.availability_zone
  tags = {
    Environment = var.subnet_environment_tag
  }
}


resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.main.id
  tags = {
      Name = "public"
    }
}

resource "aws_route" "public_internet_gateway" {
  route_table_id         = aws_route_table.public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.default.id

  depends_on = [
    aws_route_table.public_rt
  ]
}

resource "aws_route_table_association" "rta_subnet_public" {
  subnet_id      = aws_subnet.subnet_public.id
  route_table_id = aws_route_table.public_rt.id

  depends_on = [
    aws_subnet.subnet_public
  ]
}

resource "aws_network_acl" "default-network-acl" {
    vpc_id = aws_vpc.main.id
    egress {
        protocol = "-1"
        rule_no = 100
        action = "allow"
        cidr_block = "0.0.0.0/0"
        from_port = 0
        to_port = 0
    }
    ingress {
        protocol = "-1"
        rule_no = 100
        action = "allow"
        cidr_block = "0.0.0.0/0"
        from_port = 0
        to_port = 0
    }
    subnet_ids = [
        aws_subnet.subnet_public.id
    ]
    tags = {
        Name = "default ACL"
    }
}


resource "aws_security_group" "default-security-gp" {
    name = "sc-allow-all"
    vpc_id = aws_vpc.main.id
    description = "Allow all inbound traffic"
    ingress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_network_interface" "test_interface" {
  subnet_id   = aws_subnet.subnet_public.id
  private_ips = ["172.12.10.100"]

  tags = {
    Name = "test_interface"
  }
}


resource "aws_instance" "test" {
    ami = var.pavm_byol_ami_id
    #ami = "${lookup(var.pavm_payg_bun2_ami_id, var.region)}"
    availability_zone = var.availability_zone
    tenancy = "default"
    ebs_optimized = false
    disable_api_termination = false
    instance_initiated_shutdown_behavior = "stop"
    instance_type = var.pavm_instance_type
    key_name = var.pavm_key_name
    monitoring = false
    vpc_security_group_ids = [ aws_security_group.default-security-gp.id ]
    subnet_id = aws_subnet.subnet_public.id
    associate_public_ip_address = var.pavm_public_ip
    private_ip = var.pavm_mgmt_private_ip
    source_dest_check = false
    tags = {
        Name = "PAVM"
    }
    ebs_block_device {
      device_name           = "/dev/sdh"
      volume_type           = "gp2"
      volume_size           = 10
      delete_on_termination = true
    }

    connection {
        user = "admin"
        private_key = var.pavm_key_path
    }
    # bootstrap
}

# creating inventry file
resource "null_resource" "create_hostfile" {
  depends_on = [aws_instance.test]
  provisioner  "local-exec" {
    command = <<EOT
        echo [default] > hosts
        echo ${aws_instance.test.private_ip} >> hosts
        echo [default:vars] >> hosts
        echo ansible_ssh_user=admin >> hosts
        echo ansible_ssh_private_key_file=${var.pavm_key_path} >> hosts
        mv hosts ../ansible/    
EOT

  }
  
}

resource "null_resource" "execute-ansible" {
  depends_on = [null_resource.create_hostfile]
  provisioner "local-exec" {
    command = <<EOT
        sleep 120
        ANSIBLE_HOST_KEY_CHECKING=False
        ansible-playbook -i ../ansible/hosts ../ansible/main.yml    
EOT
  }
}

