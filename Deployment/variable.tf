variable "vpc_cird_base" {
  type        = string
  default = "172.12.0.0/16"
}

variable "instance_tenancy" {
  type        = string
  default     = "default"
}

variable "enable_dns_support" {
  type        = bool
  default     = true
}

variable "enable_dns_hostnames" {
  type        = bool
  default     = false
}

variable "enable_classiclink" {
  type        = bool
  default     = false
}

variable "aws_vpc_name" {
  type        = string
  default = "test_vpc"
}


variable "igw_global_tags" {
  type        = string
  default = "test_igw"
}

variable "internet_gateway_tags" {
  type        = string
  default = "test_gateway"
}


variable "cidr_subnet" {
  type        = string
  default = "172.12.0.0/24"
}

variable "availability_zone" {
  type        = string
  default     = "us-east-1a"
}

variable "subnet_environment_tag" {
  type        = string
  default = "test_subnet"
}

variable "rt_global_tags" {
  type        = string
  default = "test_rt"
}

variable "public_route_table_tags" {
  type        = string
  default = "pubilc_rt"
}

variable "pavm_byol_ami_id" {
    type = string
    default = "ami-0947d2ba12ee1ff75"
}

variable "pavm_instance_type" {
    default = "t2.micro"
}

variable "pavm_key_name" {
    description = "Name of the SSH keypair to use in AWS."
    default = "test"
}

variable "pavm_key_path" {
    description = "Path to the private portion of the SSH key specified."
    default = "C:/Users/s1039150/Downloads/test.pem"
}

variable "pavm_public_ip" {
    default = "true"
}

variable "pavm_mgmt_private_ip" {
    default = "172.12.1.1"
}