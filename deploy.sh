#!/usr/bin/env bash

javac -cp ./src/main/java ./src/main/java/com/datetime/one/CurrentDateTime.java -d ./out/
jar cvfm datetime.jar ./src/main/resources/META-INF/MANIFEST.FM -C ./out/ .

cd ./Deployment
terraform init
terraform apply -auto-approve