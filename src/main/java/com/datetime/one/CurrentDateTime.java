package main.java.com.datetime.one;
import java.time.LocalDateTime;

public class CurrentDateTime {

    public static void main(String[] arg)  {
        LocalDateTime current = LocalDateTime.now();

        System.out.println("today: " + current);
    }
    
}
